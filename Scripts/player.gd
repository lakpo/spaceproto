extends KinematicBody2D

#Datas
var thrust = 25
var thrustB = 5
var max_speed = 250
var max_vel = 250
var speedOpacity = 2.5

var pos = Vector2()
var vel = Vector2()
var acc = Vector2()
var speed = Vector2()
var screen_size = Vector2()
#rotation
var rot = 0
var rot_speed = 0
var rot_speed_max = 10
var rot_acceleration = 3
var rot_deceleration = 3

onready var bigFire = get_node("ship/BigFire")
onready var fireBR = get_node("ship/FireBR")
onready var fireBL = get_node("ship/FireBL")
onready var fireR = get_node("ship/FireR")
onready var fireL = get_node("ship/FireL")
onready var fireTR = get_node("ship/FireTR")
onready var fireTL = get_node("ship/FireTL")

onready var allFire = [ bigFire , fireTL , fireTR , fireL , fireR , fireBR , fireBL ]

func _ready():
	
	screen_size = get_viewport_rect().size
	pos = screen_size/2
	set_pos(pos)
	set_fixed_process(true)

	
func _fixed_process(delta):
	
	
	if Input.is_action_pressed("playerRotLeft"):
		rot_speed += rot_acceleration * delta
		
	elif Input.is_action_pressed("playerRotRight"):
		rot_speed -= rot_acceleration * delta
	elif rot_speed < 0.1 && rot_speed > -0.1:
		rot_speed = 0
	elif rot_speed > 0:
		rot_speed -= rot_deceleration * delta
	elif rot_speed < 0:
		rot_speed += rot_deceleration * delta
	
	rot_speed = clamp(rot_speed, -rot_speed_max , rot_speed_max )
	rot += rot_speed * delta
	set_rot(rot)
	
	#mouvements
	
	if Input.is_action_pressed("playerUp"):
		speed[1] -= thrust
	elif Input.is_action_pressed("playerDown"):
		speed[1] += thrustB
	if Input.is_action_pressed("playerLeft"):
		speed[0] = thrust *10
	elif Input.is_action_pressed("playerRight"):
		speed[0] = thrust *-10
	else :
		speed[0] = 0
	if Input.is_action_pressed("playerStop"):
		speed = Vector2(0,0)
		vel = vel - vel/50
		###################################################### Affichage Des Fires
		for cfires in allFire : 
			setVisibility( cfires , true , delta )
	else:
		if rot_speed > 0 :
			setVisibility(fireBL , false , delta )
			setVisibility(fireBR , true , delta )
		elif rot_speed < 0 :
			setVisibility(fireBL , true , delta )
			setVisibility(fireBR , false , delta )
		else:
			setVisibility(fireBL , false , delta )
			setVisibility(fireBR , false , delta )
		
		if speed[1] < 0 :
			setVisibility(bigFire ,  true , delta )
			setVisibility(fireTL , false , delta )
			setVisibility(fireTR , false , delta )
		elif speed[1] == 0 :
			setVisibility(bigFire , false , delta )
			setVisibility(fireTL , false , delta )
			setVisibility(fireTR , false , delta )
		else :
			setVisibility(bigFire , false , delta )
			setVisibility(fireTL , true , delta )
			setVisibility(fireTR , true , delta )
			
		if speed[0] < 0 :
			setVisibility(fireR , true , delta )
			setVisibility(fireL, false , delta )
		elif speed[0] == 0 :
			setVisibility(fireR , false , delta )
			setVisibility(fireL, false , delta )
		else :
			setVisibility(fireR, false , delta )
			setVisibility(fireL, true , delta )



	
	speed = speed.clamped(max_speed)
	acc = speed.rotated(rot)
	vel += acc * delta
	vel = vel.clamped(max_vel)
	pos += vel * delta
	
	
	move_to(pos)
	
func setVisibility(fire , active , delta ):
	var curOpa = fire.get_self_opacity()
	var smoke = fire.get_node("smoke")
	if active && curOpa < 1:
		curOpa += speedOpacity * delta
		smoke.set_emitting(true)
	elif curOpa > 0 :
		curOpa -= speedOpacity * delta
		smoke.set_emitting(false)
	fire.set_self_opacity(curOpa)

